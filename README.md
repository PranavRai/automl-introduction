# Introduction to AutoML

I worked on this project as part of my Research Seminar presentation to my university's NLP Group - [Current Topics in Natural Language Processing](https://www.cis.uni-muenchen.de/~fraser/topics_nlp_2020_SS/)

This repository provides a (relatively) in depth overview of AutoML, its techniques, and several popular frameworks. The advantages, pitfalls, as well as the specifics of NLP-AutoML are also discussed. I also test and benchmark 3 popular frameworks - **[TPOT](https://epistasislab.github.io/tpot/)**, **[h2o](https://docs.h2o.ai/h2o/latest-stable/h2o-docs/automl.html)**, and **[Auto-Sklearn](https://automl.github.io/auto-sklearn/master/)** on 7 Classification tasks. Another colab showcases how to use **[AutoKeras](https://autokeras.com/)** and **[Google AutoML](https://cloud.google.com/automl/)** for NLP. 

TPOT (Tree based Pipeline Optimization Tool), Auto-Sklearn, and H20 are benchmarked against a Random Search hypertuned Random 
Forest baseline. The experiments are replicated for error margins, and performance & time taken can be seen in my colab file - [AutoML_NLP_demo.ipynb](AutoML_NLP_demo.ipynb). I have run (roughly) equally timed experiments across 3 datasets from a TPOT Evaluation paper by [Olson et al. (2019)](https://link.springer.com/chapter/10.1007/978-3-030-05318-5_8), 3 datasets from the OpenML AutoML Benchmark Datasets, and  1 additional dataset from OpenML in general. All algorithms are run 3 times to allow us to report a stable mean and the standard deviation. It was found, however, that the chosen frameworks tend to very stable for these datasets. 

The second colab file - [AutoML_demo_Current_Topics_in_NLP.ipynb](AutoML_demo_Current_Topics_in_NLP.ipynb), served to underline challenges and opportunities with AutoML in NLP. Not many frameworks support NLP tasks, and those that do so offer limited support. This, and other theory is covered in the [presentation](AutoML_Presentation.pptx)

I hope that the code or analysis will help you select an AutoML best suited to your needs! I also covered the excellent paper by [Truong et al. (2019)](https://arxiv.org/abs/1908.05557) for my presentation. Interested readers should look to it for a comprehensive overview.

This project is an offshoot of an older group project which can be [found here](https://gitlab.com/PranavRai/automl-with-tpot/). The current repository, however, covers more frameworks, code, and theory.

## Instructions

Upload the ipynb files to Google Colab, or run locally on Jupyter Notebook. In the latter case, using a virtual environment is recommended. The [requirements.txt](requirements.txt) must be locally present in either case.


# References

* Olson, Randal S., et al. "Evaluation of a tree-based pipeline optimization tool for automating data science." Proceedings of the Genetic and Evolutionary Computation Conference 2016. 2016.
* Olson, Randal S., and Jason H. Moore. "TPOT: A tree-based pipeline optimization tool for automating machine learning." Automated Machine Learning. Springer, Cham, 2019. 151-160.
* Adithya Balaji and Alexander Allen, “Benchmarking Automatic Machine Learning Frameworks”. 2018. 
* Halvari, Tuomas, Jukka K. Nurminen, and Tommi Mikkonen. "Testing the Robustness of AutoML Systems." arXiv preprint arXiv:2005.02649 (2020).
* Waring, Jonathan, Charlotta Lindvall, and Renato Umeton. "Automated machine learning: Review of the state-of-the-art and opportunities for healthcare." Artificial Intelligence in Medicine (2020): 101822.
* Truong, Anh, et al. "Towards automated machine learning: Evaluation and comparison of automl approaches and tools." arXiv preprint arXiv:1908.05557 (2019).
* Chauhan, Karansingh, et al. "Automated Machine Learning: The New Wave of Machine Learning." 2020 2nd International Conference on Innovative Mechanisms for Industry Applications (ICIMIA). IEEE, 2020.